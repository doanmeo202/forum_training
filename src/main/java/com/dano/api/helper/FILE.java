package com.dano.api.helper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import org.apache.commons.io.IOUtils;

import com.dano.api.jbdc.DataSet;

public class FILE {
	public void createFileFromDB(String url, String filename, String fileType) throws Exception {
		DataSet ds = DataSet.getDataSet();
		String fileEx = ds.get999Config(fileType.toUpperCase());
		URL cssUrl = new URL(url);
		InputStream in = cssUrl.openStream();
		File file = new File("/main/WebContent/WEB-INF/public/css/" + filename + fileEx);
		FileOutputStream out = new FileOutputStream(file);
		IOUtils.copy(in, out);
		in.close();
		out.close();
	}

	
}
