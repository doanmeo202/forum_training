package com.dano.api.html.template;

import java.util.List;
import java.util.Map;

import com.dano.api.helper.FILE;
import com.dano.api.jbdc.DataSet;

public class Config {
	private String header;
	private String link;
	private String body;
	private String footer;

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public void setupPage(String parner, String header, String link, String body, String footer) {
		DataSet ds = DataSet.getDataSet();
		ds.setField("PARNER", header);
		ds.setField("HEADER", header);
		ds.setField("LINK", link);
		ds.setField("BODY", body);
		ds.setField("FOOTER", footer);
		String Sql = "INSERT INTO DB_FORUM.HTML_TEMPLATE (PARNER, HEADER, LINK, BODY, FOOTER) VALUES(:PARNER, :HEADER, :LINK, :BODY, :FOOTER);";
		try {
			ds.update(Sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<Map<String, Object>> getHTML(String PARNER) {
//		/HTML_CONFIG
		DataSet ds = DataSet.getDataSet();
		ds.setField("PARNER", PARNER);
		String Sql = "SELECT * FROM HTML_CONFIG WHERE PARNER=:PARNER";
		List<Map<String, Object>> li = ds.searchAndRetrieve(Sql);
		return li;

	}

	public static void main(String[] args) {
		List<Map<String, Object>> li = getHTML("FORUM");
		for (Map<String, Object> map : li) {
			System.out.println(new HTML().getJsCss(map.get("type").toString(), map.get("value").toString()));
		}

		// getHTMLTAGFILE;
	}
}
