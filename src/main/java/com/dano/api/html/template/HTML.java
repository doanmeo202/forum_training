package com.dano.api.html.template;

import org.jsoup.nodes.Element;

public class HTML {
	public String getJsCss(String type, String href) {
		Element node = null;
		if (type.toUpperCase().startsWith("C")) {
			node = new Element("link");
			node.attr("rel", "stylesheet");
			node.attr("type", "text/css");
			node.attr("href", href);
		} else if (type.toUpperCase().startsWith("J")) {
			node = new Element("script");
			node.attr("type", "text/javascript");
			node.attr("src", href);
		}
		if (node == null) {
			return "";
		}
		return node.toString();
	}
}
