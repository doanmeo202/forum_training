package com.dano.api.config;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.dano.api.jbdc.DataAutoWrite;

@Configuration
@ComponentScan("com.dano.api.*")
public class ApplicationContextConfig {

	@Bean(name = "viewResolver")
	public InternalResourceViewResolver getViewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/public/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver commonsMultipartResolver() {
		final CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
		commonsMultipartResolver.setMaxUploadSize(-1);
		return commonsMultipartResolver;
	}

	@Bean(name = "configLog4j")
	public PropertyConfigurator configLog4j() {
		return new Enviroment().configLog4J();
	}
	/*
	 * @Bean(name = "createSQLSTRING") public boolean createSQLSTRING() { return new
	 * autoGetMapCodeSql().createSQLSTRING(); }
	 */
	
}
