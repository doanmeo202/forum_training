package com.dano.api.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.dano.api.jbdc.DataSet;
import com.mysql.fabric.xmlrpc.base.Data;

/*@Author: DoanNgocThanh
 *Gitlab: https://gitlab.com/doanmeo202 
 *Version: 1.0*/
public class Enviroment {
	private static Logger log = Logger.getLogger(Enviroment.class);
	private static DataSet ds;
	private static boolean STAG = false;
	private static boolean PROD = false;
	private static String pathFileConfig;
	private static Properties SYS_PROPERTIES = System.getProperties();
	private static String OS_NAME = SYS_PROPERTIES.get("os.name").toString();
	public static String IP_ENV;
	public static String USERNAME_DB;
	public static String PASSWORD_DB;
	public static String DATABASE_DB;
	public static String URL_CONNECT_DB;
	public static String STRING_ENVIROMENT;
	public static Map<String, String> DATABASE_CONFIG;
	public static String url_local;
	public static String url_prod;
	public static String username_prod;
	public static String password_prod;
	public static String username_local;
	public static String password_local;
	public static String FIREBASE_PARAM;
	public static String CODEMAP_TABLE;
	public static String SYS_BOT_TOKEN;
	public static String SYS_BOT_USERNA;
	public static String SYS_BOT_CHATID;
	public static String MAIL_ACCOUNT_SYS;
	public static String DB_SEVER;
	public static Date date = new Date();
	public static SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");
	public static String dateLog = inputFormat.format(date);
	static String logImage = "                 \\||/\r\n" + "                 |  @___oo\r\n"
			+ "       /\\  /\\   / (__,,,,|\r\n" + "      ) /^\\) ^\\/ _)\r\n" + "      )   /^\\/   _)\r\n"
			+ "      )   _ /  / _)\r\n" + "  /\\  )/\\/ ||  | )_)\r\n" + " <  >      |(,,) )__)\r\n"
			+ "  ||      /    \\)___)\\\r\n" + "  | \\____(      )___) )___\r\n" + "   \\______(_______;;; __;;;";

	public static void logWrite(int line, String text) {
		System.err.println(dateLog + " [" + Enviroment.class.getSimpleName() + "] static (" + line + ") :" + text);
		log.debug("[" + Enviroment.class.getSimpleName() + "] static (" + line + ") :" + text);
	}

	public static void logWrite(Object classZ, int line, String text) {
		System.err.println(dateLog + " [" + classZ.getClass().getSimpleName() + "] (" + line + ") :" + text);
		log.debug("[" + classZ.getClass().getSimpleName() + "] (" + line + ") :" + text);
	}

	public synchronized PropertyConfigurator configLog4J() {
		PropertyConfigurator configurer = new PropertyConfigurator();
		BasicConfigurator.configure();
		Properties proper = new Properties();
		proper.setProperty("log4j.rootCategory", "DEBUG, all");
		proper.setProperty("log4j.appender.all", "org.apache.log4j.ConsoleAppender");
		proper.setProperty("log4j.appender.all.layout", "org.apache.log4j.PatternLayout");
		proper.setProperty("log4j.appender.all.layout.ConversionPattern",
				"%d{yyyy-MM-dd HH:mm:ss,SSS} [%-5p][%C{1}.%M()(%L)] - %m%n");
		configurer.configure(proper);
		return configurer;
	}

	static {
		// config Log4j
		BasicConfigurator.configure();
		Properties proper = new Properties();
		proper.setProperty("log4j.rootCategory", "DEBUG, all");
		proper.setProperty("log4j.appender.all", "org.apache.log4j.ConsoleAppender");
		proper.setProperty("log4j.appender.all.layout", "org.apache.log4j.PatternLayout");
		proper.setProperty("log4j.appender.all.layout.ConversionPattern",
				"%d{yyyy-MM-dd HH:mm:ss,SSS} [%-5p][%C{1}.%M()(%L)] - %m%n");
		PropertyConfigurator.configure(proper);
		// config database system project
		System.out.println(logImage);
		System.err.println(
				"===========================================================================================================================");
		DATABASE_CONFIG = new HashMap<String, String>();
		CODEMAP_TABLE = "DTCM0004_CODEMAPSQL";

		if (OS_NAME.startsWith("W")) {// check OS
			STAG = true;
			PROD = false;
			STRING_ENVIROMENT = "STAG";
		} else {
			STAG = false;
			PROD = true;
			pathFileConfig = "/opt/tomcat/conf/database.properties";
			STRING_ENVIROMENT = "PROD";
		}
		logWrite(Thread.currentThread().getStackTrace()[1].getLineNumber(), "PATH_FILE_CONFIG > " + pathFileConfig);
		logWrite(Thread.currentThread().getStackTrace()[1].getLineNumber(), "STRING_ENVIROMENT>" + STRING_ENVIROMENT);

		DB_SEVER = "mysql";

		if (STAG) {
			DATABASE_CONFIG.put("username", "cxluser");
			DATABASE_CONFIG.put("password", "Thanh0974135042!");
			DATABASE_CONFIG.put("database", "DB_FORUM");
			DATABASE_CONFIG.put("host", "192.168.31.223");
			DATABASE_CONFIG.put("port", "3306");

		}
		if (PROD) {
			Properties properties = new Properties();
			properties = getPropertiesFile(pathFileConfig);
			DATABASE_CONFIG.put("username", properties.getProperty("username"));
			DATABASE_CONFIG.put("password", properties.getProperty("password"));
			DATABASE_CONFIG.put("database", properties.getProperty("database"));
			DATABASE_CONFIG.put("host", properties.getProperty("host"));
			DATABASE_CONFIG.put("port", properties.getProperty("port"));
		}
		if (DB_SEVER.equals("mysql")) {
			DATABASE_CONFIG.put("drive", "com.mysql.cj.jdbc.Driver");
			URL_CONNECT_DB = "jdbc:" + DB_SEVER + "://" + DATABASE_CONFIG.get("host").toString() + ":"
					+ DATABASE_CONFIG.get("port").toString() + "/" + DATABASE_CONFIG.get("database").toString()
					+ "?useUnicode=yes&characterEncoding=UTF-8";
		} else if (DB_SEVER.equals("sqlserver")) {
			DATABASE_CONFIG.put("drive", "..................");
		}

		DATABASE_CONFIG.put("url", URL_CONNECT_DB);
		USERNAME_DB = DATABASE_CONFIG.get("username").toString();
		PASSWORD_DB = DATABASE_CONFIG.get("password").toString();
		DATABASE_DB = DATABASE_CONFIG.get("database").toString();

		if (DataSet.pingConnection()) {
			// Get From Config DB
			ds = DataSet.getDataSet();
			ds.setupNewEnv();
			IP_ENV = ds.get999Config("IP_" + STRING_ENVIROMENT);
			FIREBASE_PARAM = ds.get999Config("FIREBASE_PARAM_" + STRING_ENVIROMENT);
			SYS_BOT_TOKEN = ds.get999Config("SYS_BOT_TOKEN_" + STRING_ENVIROMENT);
			SYS_BOT_USERNA = ds.get999Config("SYS_BOT_USERNAME_" + STRING_ENVIROMENT);
			SYS_BOT_CHATID = ds.get999Config("SYS_BOT_CHATID_" + STRING_ENVIROMENT);
			MAIL_ACCOUNT_SYS = ds.get999Config("MAIL_ACCOUNT_SYS_" + STRING_ENVIROMENT);
			username_local = ds.get999Config("username_local");
			password_local = ds.get999Config("password_local");
			url_local = ds.get999Config("url_local");
			username_prod = ds.get999Config("username_prod");
			password_prod = ds.get999Config("password_prod");
			url_prod = ds.get999Config("url_prod");

		} else {
			System.err.println(
					dateLog + " [ERROR][Enviroment(" + Thread.currentThread().getStackTrace()[1].getLineNumber()
							+ ")] : connection database error please check!");
		}
		System.err.println(
				"---------------------------------------------------------------------------------------------------------------------------");

		logWrite(Thread.currentThread().getStackTrace()[1].getLineNumber(),
				"Config Enviroment " + STRING_ENVIROMENT + " is complete!");
		System.err.println(
				"===========================================================================================================================");
	}

	public static synchronized Properties getPropertiesFile(String filePath) {
		Properties props = new Properties();
		FileInputStream fis;
		try {
			fis = new FileInputStream(filePath);
			props.load(fis);
		} catch (IOException e) {
			System.out.println("Not found " + filePath);
			e.printStackTrace();
			return null;
		}
		return props;
	}

	public static void main(String[] args) {
		BasicConfigurator.configure();
		Enviroment e = new Enviroment();
		DataSet ds = DataSet.getDataSet();
		// (:USERNAME, :PASSWORD, :FULLNAME, 0, 0, :EMAIL
		ds.setField("USERNAME", "xthanhxvn4");
		ds.setField("PASSWORD", "AAAAAAAAA");
		ds.setField("FULLNAME", "Nguyễn Văn Tèo");
		ds.setField("EMAIL", "xthanhxvn4@gmail,com");
		try {
			ds.update("insertUserForum");
		} catch (Exception e2) {
		}
		//System.out.println(ds.searchAndRetrieve("getUserInForumByUsername"));

	}
}
