package com.dano.api.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
public class WebMvcConfig extends WebMvcConfigurerAdapter {

 
 
   @Override
   public void addResourceHandlers(ResourceHandlerRegistry registry) {
       registry.addResourceHandler("/**").addResourceLocations("/WEB-INF/public/").setCachePeriod(31556926);
//       registry.addResourceHandler("/images/**").addResourceLocations("/WEB-INF/public/images/").setCachePeriod(31556926);
//       registry.addResourceHandler("/js/**").addResourceLocations("/WEB-INF/public/js/").setCachePeriod(31556926);
//       registry.addResourceHandler("/.sass-cache/**").addResourceLocations("/WEB-INF/public/.sass-cache/").setCachePeriod(31556926);
//       registry.addResourceHandler("/fonts/**").addResourceLocations("/WEB-INF/public/fonts/").setCachePeriod(31556926);
//       registry.addResourceHandler("/sass/**").addResourceLocations("/WEB-INF/public/sass/").setCachePeriod(31556926);

   }

   
   @Override
   public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
       configurer.enable();
   }

}