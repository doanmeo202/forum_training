package com.dano.api.index.mod;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;

import com.dano.api.html.template.Config;
import com.dano.api.html.template.HTML;
import com.dano.api.jbdc.DataSet;

public class index_mod {
	public List<String> getListLinkBostrap(String webUsed) {
		HTML html = new HTML();
		List<String> list = new ArrayList();
		List<Map<String, Object>> li = new Config().getHTML(webUsed);
		for (Map<String, Object> map : li) {
			list.add(html.getJsCss(ObjectUtils.toString(map.get("type")), ObjectUtils.toString(map.get("value"))));

		}
		System.out.println(list);
		return list;
	}

	public String getTitle(String webUsed, String url) {
		String titleDef = "DanoForum";
		DataSet ds = DataSet.getDataSet();
		ds.setField("PARNER", webUsed);
		ds.setField("PARAM", url);
		List<Map<String, Object>> li = ds
				.searchAndRetrieve(" SELECT * FROM HTML_CONFIG where PARNER =:PARNER and PARAM =:PARAM");
		for (Map<String, Object> map : li) {
			String title = ObjectUtils.toString(map.get("value"));
			if (!"".equals(title)) {
				titleDef = title;
			}
		}
		return titleDef;
	}

	public List<Map<String, Object>> getListCategory(String webUsed) {
		List<Map<String, Object>> list = new ArrayList();
		DataSet ds = DataSet.getDataSet();
		ds.setField("PARNER", webUsed);
		list = ds.searchAndRetrieve("SELECT * FROM CATEGORY_POST where PARNER =:PARNER");
		System.out.println(list);
		return list;

	}

	public static void insertPost(String ID_POST, String POST_TITLE, String POST_CONTENT, String POST_VIEW,
			String POST_AUTH, String POST_UPDATE_TIME) {
		String sql = "INSERT INTO FORUM_POST (ID_POST, POST_TITLE, POST_CONTENT, POST_VIEW, POST_AUTH, POST_UPDATE_TIME) VALUES(:ID_POST, :POST_TITLE, :POST_CONTENT, :POST_VIEW,:POST_AUTH , :POST_UPDATE_TIME);";
		DataSet ds = DataSet.getDataSet();
		ds.setField("ID_POST", ID_POST);
		ds.setField("POST_TITLE", POST_TITLE);
		ds.setField("POST_CONTENT", POST_CONTENT);
		ds.setField("POST_VIEW", POST_VIEW);
		ds.setField("POST_AUTH", POST_AUTH);
		ds.setField("POST_UPDATE_TIME", POST_UPDATE_TIME);
		try {
			ds.update(sql);
			System.err.println("success!!!!");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public static List<Map<String, Object>> getPOST() {
		List<Map<String, Object>> list = new ArrayList();
		DataSet ds = DataSet.getDataSet();
		// ds.setField("PARNER", webUsed);
		list = ds.searchAndRetrieve("SELECT * FROM FORUM_POST");
		System.out.println(list);
		return list;

	}

	public static void main(String[] args) {
		List<Map<String, Object>> list = getPOST();
		for (Map<String, Object> map : list) {
			System.out.println(map.get("id_post"));

		}

//		for (int i = 0; i < 10; i++) {
//			insertPost("ID" + i, "POST_TITLE" + i, "POST_CONTENT" + i, "POST_VIEW" + i, "POST_AUTH" + i, null);
//		}
	}
}
