package com.dano.api.index.trx;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dano.api.index.mod.index_mod;

@Controller
public class index {
	private String thisWeb = "FORUM";

	@RequestMapping("/")
	public String indexPage(@RequestParam Map<String, Object> params, HttpServletRequest request, Model model) {
		model = getModel(request, model);
		return "index";
	}

	public Model getModel(HttpServletRequest request, Model model) {
		index_mod mod = new index_mod();
		String url = request.getRequestURI();
		model.addAttribute("links", mod.getListLinkBostrap(thisWeb));
		model.addAttribute("title", mod.getTitle(thisWeb, url));
		model.addAttribute("categorys", mod.getListCategory(thisWeb));
		return model;
	}

	@RequestMapping(value = "/v1/**", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public void service(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, Object> params) throws ServletException, IOException {
		String ipAddressClient = ObjectUtils.toString(request.getHeader("X-Real-IP"));
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		String url = request.getRequestURI();
		int indexUrl = url.indexOf("/api/");

	}

}
