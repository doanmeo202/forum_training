package com.dano.api.jbdc;

import java.lang.String;
import java.util.List;

public class DTZSAP01 {
	private String API_KEY;

	private String DESCRIPTION;

	private String URI_PATH;

	private String LST_PROC_ID;

	private String LST_PROC_DATE;

	private String OP_DIV_NO;

	private String COMP_ID;

	private String NEED_LOG;

	private String NEED_CHECK;

	public String getAPI_KEY() {
		return this.API_KEY;
	}

	public void setAPI_KEY(String API_KEY) {
		this.API_KEY = API_KEY;
	}

	public String getDESCRIPTION() {
		return this.DESCRIPTION;
	}

	public void setDESCRIPTION(String DESCRIPTION) {
		this.DESCRIPTION = DESCRIPTION;
	}

	public String getURI_PATH() {
		return this.URI_PATH;
	}

	public void setURI_PATH(String URI_PATH) {
		this.URI_PATH = URI_PATH;
	}

	public String getLST_PROC_ID() {
		return this.LST_PROC_ID;
	}

	public void setLST_PROC_ID(String LST_PROC_ID) {
		this.LST_PROC_ID = LST_PROC_ID;
	}

	public String getLST_PROC_DATE() {
		return this.LST_PROC_DATE;
	}

	public void setLST_PROC_DATE(String LST_PROC_DATE) {
		this.LST_PROC_DATE = LST_PROC_DATE;
	}

	public String getOP_DIV_NO() {
		return this.OP_DIV_NO;
	}

	public void setOP_DIV_NO(String OP_DIV_NO) {
		this.OP_DIV_NO = OP_DIV_NO;
	}

	public String getCOMP_ID() {
		return this.COMP_ID;
	}

	public void setCOMP_ID(String COMP_ID) {
		this.COMP_ID = COMP_ID;
	}

	public String getNEED_LOG() {
		return this.NEED_LOG;
	}

	public void setNEED_LOG(String NEED_LOG) {
		this.NEED_LOG = NEED_LOG;
	}

	public String getNEED_CHECK() {
		return this.NEED_CHECK;
	}

	public void setNEED_CHECK(String NEED_CHECK) {
		this.NEED_CHECK = NEED_CHECK;
	}

	public static void main(String[] args) {
		DataSet ds = DataSet.getDataSet();
		List<DTZSAP01> list = (List<DTZSAP01>) ds.searchAndRetrieve("Select * FROM DTZSAP01", new DTZSAP01());
		for (DTZSAP01 dtzsap01 : list) {
			System.out.println(dtzsap01.getURI_PATH());
		}
	}
}
