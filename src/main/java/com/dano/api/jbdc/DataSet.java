package com.dano.api.jbdc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;
import com.dano.api.config.Enviroment;

/*@Author: DoanNgocThanh
 *Version: 1.0*/
public class DataSet {
	private static Logger logger = Logger.getLogger(DataSet.class);
	private static String tableCodeMap = "DTCM0004_CODEMAPSQL";
	private static String tableA999CONFIG = "A999_CONFIG";
	private Sql2o batchSql2o;
	public Query query;
	private Connection connection;
	private static Map<String, Object> mapSetField = new HashMap<String, Object>();
	static {
		try {
			Class.forName(ObjectUtils.toString(Enviroment.DATABASE_CONFIG.get("drive")));
			// Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			throw new Sql2oException("not found driver!");
		}
	}

	public synchronized static boolean pingConnection() {
		boolean ping = false;
		try {
			DataSet ds = DataSet.getDataSet();
			ds.beginTransaction();
			ping = true;

		} catch (Exception e) {
			ping = false;
		}

		return ping;
	}

	public boolean isExitsTable(String Table) {
		boolean check = true;
		DataSet ds = DataSet.getDataSet();
		ds.setField("TABLE", Table);
		if (ds.searchAndRetrieve("SHOW TABLES LIKE :TABLE").size() > 0) {
			check = false;
		}
		return check;
	}

	public synchronized void setupNewEnv() {
		String DDL_DTCM0004_CODEMAPSQL = "CREATE TABLE " + tableCodeMap.toUpperCase()
				+ " (CODE_NAME varchar(150) NOT NULL,SQLSTRING text, FLAG smallint DEFAULT '0',PRIMARY KEY (CODE_NAME));";
		if (isExitsTable(tableCodeMap.toUpperCase())) {
			try {
				update(DDL_DTCM0004_CODEMAPSQL);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String DDL_A999CONFIG = "CREATE TABLE " + tableA999CONFIG.toUpperCase()
				+ " (PARAM_NAME varchar(100)  NOT NULL, PARAM_VALUE text  NOT NULL,DESCRIPTION varchar(100) DEFAULT NULL,PRIMARY KEY (PARAM_NAME));";
		if (isExitsTable(tableA999CONFIG.toUpperCase())) {
			try {
				update(DDL_A999CONFIG);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public Connection beginTransaction() {
		batchSql2o.open();
		connection = batchSql2o.beginTransaction();
		logger.fatal("beginTransaction");
		return connection;
	}

	public Connection rollbackTransaction() {
		connection.rollback();
		logger.fatal("rollbackTransaction");
		return connection;
	}

	public Connection endTransaction() {
		if (connection != null) {
			connection.close();
			logger.fatal("endTransaction");
		}
		return connection;
	}

	public static synchronized DataSet getDataSetPROD() {
		DataSet dataSet = new DataSet();
		try {
			dataSet.batchSql2o = new Sql2o(Enviroment.url_prod, Enviroment.username_prod, Enviroment.password_prod);
		} catch (Exception e) {
			throw e;
		}
		return dataSet;
	}

	public static synchronized DataSet getDataSet(String url, String username, String password) {
		DataSet dataSet = new DataSet();
		dataSet.batchSql2o = new Sql2o(url, username, password);
		return dataSet;
	}

	public static synchronized DataSet getDataSet() {
		DataSet dataSet = new DataSet();
		dataSet.batchSql2o = new Sql2o(Enviroment.URL_CONNECT_DB, Enviroment.USERNAME_DB, Enviroment.PASSWORD_DB);
		return dataSet;
	}

	public Query createQuery(Connection con, String sql) {
		query = con.createQuery(sql);
		return query;
	}

	public static void clear() {
		mapSetField.clear();
	}

	public static void setField(String key, String value) {
		mapSetField.put(key, value);
	}

	public Query setField(String name, Object value) {
		try {
			query.addParameter(name, value);
		} catch (Exception e) {
			logger.debug("setField Failed: [" + name + "] - [" + value + "]");
		}
		return query;
	}

	public List<Map<String, Object>> getSQL(String keysql) {
		Connection con = batchSql2o.beginTransaction();
		// Connection con = beginTransaction();
		String sql = "";
		List<Map<String, Object>> map1 = new ArrayList();
		try {
			map1 = con.createQuery("SELECT * from " + tableCodeMap + " where CODE_NAME like :CODE_NAME")
					.addParameter("CODE_NAME", keysql).executeAndFetchTable().asList();

		} catch (Exception e) {
			con.rollback();
		} finally {
			con.close();
		}
		return map1;
	}

	public List<? extends Object> searchAndRetrieve(String keysql, Object obj) {
		List<? extends Object> list = new ArrayList();
		Connection con = batchSql2o.beginTransaction();
		String sql = "";
		List<Map<String, Object>> map1 = new ArrayList();
		try {
			map1 = con.createQuery("SELECT * from " + tableCodeMap + " where CODE_NAME like :CODE_NAME")
					.addParameter("CODE_NAME", keysql).executeAndFetchTable().asList();
			if (map1.size() > 0) {
				for (Map<String, Object> map : map1) {
					sql = ObjectUtils.toString(map.get("sqlstring"), "");
				}
			} else {
				sql = keysql;
			}
			query = createQuery(con, sql);
			if (mapSetField != null || mapSetField.size() > 0) {
				ArrayList myKeyList = new ArrayList(mapSetField.keySet());

				for (int i = 0; i < myKeyList.size(); i++) {
					String key = (String) myKeyList.get(i);
					String value = (String) mapSetField.get(myKeyList.get(i));
					try {
						query.addParameter(key, value);
						logger.fatal("SET PARAM [" + key + "=" + value + "]");
					} catch (Exception e) {

					}
				}

			}
			try {
				list = query.executeAndFetch(obj.getClass());
			} catch (Exception e) {
				con.rollback();
				logger.error("ERROR: ", e);
			}
		} finally {
			con.close();
		}
		return list;
	}

	public List<Map<String, Object>> getSQL() {
		Connection con = batchSql2o.beginTransaction();
		// Connection con = beginTransaction();
		String sql = "";
		List<Map<String, Object>> map1 = new ArrayList();
		try {
			map1 = con.createQuery("SELECT * from " + tableCodeMap).executeAndFetchTable().asList();

		} catch (Exception e) {
			con.rollback();
		} finally {
			con.close();
		}
		return map1;
	}

	public List<Map<String, Object>> searchAndRetrieve(String keysql) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Connection con = batchSql2o.beginTransaction();
		String sql = "";
		List<Map<String, Object>> map1 = new ArrayList();
		try {
			map1 = con.createQuery("SELECT * from " + tableCodeMap + " where CODE_NAME like :CODE_NAME")
					.addParameter("CODE_NAME", keysql).executeAndFetchTable().asList();
			if (map1.size() > 0) {
				for (Map<String, Object> map : map1) {
					sql = ObjectUtils.toString(map.get("sqlstring"), "");
				}
			} else {
				sql = keysql;
			}
			query = createQuery(con, sql);
			if (mapSetField != null || mapSetField.size() > 0) {
				ArrayList myKeyList = new ArrayList(mapSetField.keySet());

				for (int i = 0; i < myKeyList.size(); i++) {
					String key = (String) myKeyList.get(i);
					String value = (String) mapSetField.get(myKeyList.get(i));
					try {
						query.addParameter(key, value);
						logger.fatal("SET PARAM [" + key + "=" + value + "]");
					} catch (Exception e) {

					}
				}

			}
			try {
				list = query.executeAndFetchTable().asList();
			} catch (Exception e) {
				con.rollback();
				logger.error("ERROR: ", e);
			}
		} finally {
			con.close();
		}
		return list;
	}

	public void update(String keysql) throws Exception {
		Connection con = batchSql2o.beginTransaction();
		String sql = "";
		List<Map<String, Object>> map1 = new ArrayList();
		try {
			map1 = con.createQuery("SELECT * from " + tableCodeMap + " where CODE_NAME like :CODE_NAME")
					.addParameter("CODE_NAME", keysql).executeAndFetchTable().asList();
			if (map1.size() > 0) {
				for (Map<String, Object> map : map1) {
					sql = ObjectUtils.toString(map.get("sqlstring"), "");
				}
			} else {
				sql = keysql;
			}
			query = createQuery(con, sql);
			if (mapSetField != null || mapSetField.size() > 0) {
				ArrayList myKeyList = new ArrayList(mapSetField.keySet());

				for (int i = 0; i < myKeyList.size(); i++) {
					String key = (String) myKeyList.get(i);
					String value = (String) mapSetField.get(myKeyList.get(i));
					try {
						query.addParameter(key, value);
						logger.fatal("SET PARAM [" + key + "=" + value + "]");
					} catch (Exception e) {

					}
				}

			}
			try {
				query.executeUpdate().commit();
			} catch (Exception e) {
				con.rollback();
				e.printStackTrace();
				throw e;
			}
		} finally {
			con.close();
		}

	}

	public String get999Config(String paramName) {
		DataSet ds = getDataSet();
		ds.clear();
		ds.setField("PARAM_NAME", paramName);
		String data = "";
		List<Map<String, Object>> lids = ds.searchAndRetrieve(
				"select PARAM_VALUE from " + tableA999CONFIG.toUpperCase() + " c where PARAM_NAME =:PARAM_NAME");
		for (Map<String, Object> map : lids) {
			data = ObjectUtils.toString(map.get("param_value"), "");
		}
		return data;

	}

	public void set999Config(String key, String value) {
		String sql = "INSERT INTO " + tableA999CONFIG.toUpperCase()
				+ " (PARAM_NAME, PARAM_VALUE, DESCRIPTION)VALUES(:KEY, :VALUE, NULL);";
		DataSet ds = getDataSet();
		ds.clear();
		ds.setField("KEY", key);
		ds.setField("VALUE", value);
		try {
			ds.update(sql);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static void main(String[] args) {
		BasicConfigurator.configure();
		DataSet ds = DataSet.getDataSet();
		ds.setupNewEnv();
		ds.setField("TABLE", "A999_CONFIG");
//		try {
//			ds.update("INSERT INTO db_stag.cxlaa_aab1_0100_ip"
//					+ "(IP, PATH, IS_LOCK, LOGS, TIME_LOGS, IP_INPUT, NGINX_FILE)"
//					+ "VALUES('11', '1:8080/Webdano/', 0, NULL, NULL, NULL, NULL);");
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		logger.debug(ds.isExitsTable("A999CONFIG"));
		ds.clear();
		ds.set999Config("123456", "789101112");
	}
}
