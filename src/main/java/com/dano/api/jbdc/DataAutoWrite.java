package com.dano.api.jbdc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;

import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeSpec.Builder;

public class DataAutoWrite {
	private List<Map<String, Object>> listCodeMapSql;
	private DataSet ds;
	private static List<FieldSpec> liFieldSpec;
	private static List<MethodSpec> liMethodSpec;
	private static javax.lang.model.element.Modifier PRIVATE = javax.lang.model.element.Modifier.PRIVATE;
	private static javax.lang.model.element.Modifier PUBLIC = javax.lang.model.element.Modifier.PUBLIC;
	private static javax.lang.model.element.Modifier SYNCHRONIZED = javax.lang.model.element.Modifier.SYNCHRONIZED;
	private static javax.lang.model.element.Modifier STATIC = javax.lang.model.element.Modifier.STATIC;

	public FieldSpec nameField(String key) {
		FieldSpec nameField = FieldSpec.builder(String.class, key).addModifiers(PRIVATE).build();
		return nameField;
	}/*
		*/

	private List<Map<String, Object>> getListCodeMapSql() {
		ds = DataSet.getDataSet();
		listCodeMapSql = new ArrayList();
		listCodeMapSql = ds.getSQL();
		return listCodeMapSql;
	}

	public static FieldSpec nameField(String key, String value) {
		FieldSpec nameField = null;
		try {
			nameField = FieldSpec.builder(String.class, key).addModifiers(PUBLIC, STATIC).initializer("$S", value)
					.build();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return nameField;
	}
	public static void main(String[] args) {
		liFieldSpec = new ArrayList<FieldSpec>();
		liMethodSpec = new ArrayList<MethodSpec>();
		DataSet ds = DataSet.getDataSet();
		List<Map<String, Object>> listCodeMapSql = ds.getSQL();
		for (Map<String, Object> map : listCodeMapSql) {
			String code_name = ObjectUtils.toString(map.get("code_name")).replace(".", "");
			String code_name_norep = ObjectUtils.toString(map.get("code_name"));
			String sqlstring = "/*" + ObjectUtils.toString(map.get("sqlstring")) + "*/";
			String javacode = "private String " + code_name + " =\"" + code_name_norep + "\"; " + sqlstring;
			FieldSpec nameField = nameField(code_name, code_name_norep);
			liFieldSpec.add(nameField);
		}
		String className = "SQL_STRING";
		Builder classType = TypeSpec.classBuilder(className);
		for (FieldSpec fieldSpec : liFieldSpec) {
			classType.addField(fieldSpec);
		}
		MethodSpec setNameMethod = MethodSpec.methodBuilder(className).addModifiers(PUBLIC, SYNCHRONIZED).build();
		classType.addMethod(setNameMethod);
		TypeSpec personClass_ = classType.addModifiers(javax.lang.model.element.Modifier.PUBLIC).build();
		String absolutePath = System.getProperty("user.dir") + "/src/";
		if (personClass_ != null) {
			String voPath = "com.dano.api.jbdc";
			JavaFile javaFile = JavaFile.builder(voPath, personClass_).build();
			try {
				File file = new File(absolutePath);
				javaFile.writeTo(file);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.err.println("null rồi");
		}
		// System.out.println(SQL_STRING.comdanowebappaab0modunAAB0_0100_GET_BANNER);

	}

	public boolean createSQLSTRING() {
		boolean check = false;
		liFieldSpec = new ArrayList<FieldSpec>();
		liMethodSpec = new ArrayList<MethodSpec>();
		DataSet ds = DataSet.getDataSet();
		List<Map<String, Object>> listCodeMapSql = ds.getSQL();
		for (Map<String, Object> map : listCodeMapSql) {
			String code_name = ObjectUtils.toString(map.get("code_name")).replace(".", "");
			String code_name_norep = ObjectUtils.toString(map.get("code_name"));
			String sqlstring = "/*" + ObjectUtils.toString(map.get("sqlstring")) + "*/";
			String javacode = "private String " + code_name + " =\"" + code_name_norep + "\"; " + sqlstring;
			FieldSpec nameField = nameField(code_name, code_name_norep);
			liFieldSpec.add(nameField);
		}
		String className = "SQL_STRING";
		Builder classType = TypeSpec.classBuilder(className);
		for (FieldSpec fieldSpec : liFieldSpec) {
			classType.addField(fieldSpec);
		}
		MethodSpec setNameMethod = MethodSpec.methodBuilder(className).addModifiers(PUBLIC, SYNCHRONIZED).build();
		classType.addMethod(setNameMethod);
		TypeSpec personClass_ = classType.addModifiers(javax.lang.model.element.Modifier.PUBLIC).build();
		String absolutePath = System.getProperty("user.dir") + "/src/";
		if (personClass_ != null) {
			String voPath = "com.dano.api.jbdc";
			JavaFile javaFile = JavaFile.builder(voPath, personClass_).build();
			try {
				File file = new File(absolutePath);
				javaFile.writeTo(file);
				check = true;
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>FILE NE: " + file.getAbsolutePath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.err.println("null rồi");
		}
		return check;
	}

}
