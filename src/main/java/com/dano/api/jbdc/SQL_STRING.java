package com.dano.api.jbdc;

import java.lang.String;

public class SQL_STRING {
  public static String comdanowebappaab0modunAAB0_0100_GET_ABOUTUS1 = "com.dano.web.app.aa.b0.modun.AAB0_0100_GET_ABOUTUS1";

  public static String comdanowebappaab0modunAAB0_0100_GET_ABOUTUS2 = "com.dano.web.app.aa.b0.modun.AAB0_0100_GET_ABOUTUS2";

  public static String comdanowebappaab0modunAAB0_0100_GET_BANNER = "com.dano.web.app.aa.b0.modun.AAB0_0100_GET_BANNER";

  public static String comdanowebappaab0modunAAB0_0100_GET_MENU = "com.dano.web.app.aa.b0.modun.AAB0_0100_GET_MENU";

  public static String comdanowebappaab0modunAAB0_0100_GET_SEVICE = "com.dano.web.app.aa.b0.modun.AAB0_0100_GET_SEVICE";

  public static String comdanowebappaab0modunAAB0_0100_GET_SEVICE1 = "com.dano.web.app.aa.b0.modun.AAB0_0100_GET_SEVICE1";

  public static String comdanowebappaab0modunAAB0_0100_GET_SEVICE2 = "com.dano.web.app.aa.b0.modun.AAB0_0100_GET_SEVICE2";

  public static String comdanowebappaab0modunAAB0_0100_GET_SUB_MENU = "com.dano.web.app.aa.b0.modun.AAB0_0100_GET_SUB_MENU";

  public static String comdanowebappaab0modunAAB0_0101_BACKUP_CODEMAP = "com.dano.web.app.aa.b0.modun.AAB0_0101_BACKUP_CODEMAP";

  public static String comdanowebappaab0modunAAB0_0101_GET_CATEGORY = "com.dano.web.app.aa.b0.modun.AAB0_0101_GET_CATEGORY";

  public static String comdanowebappaab0modunAAB0_0101_GET_FIELD_OPTION_LIST = "com.dano.web.app.aa.b0.modun.AAB0_0101_GET_FIELD_OPTION_LIST";

  public static String comdanowebappaab0modunAAB1_0100_GET_MENU = "com.dano.web.app.aa.b0.modun.AAB1_0100_GET_MENU";

  public static String comdanowebappaab1modunAAB1_0100_GET_IP = "com.dano.web.app.aa.b1.modun.AAB1_0100_GET_IP";

  public static String comdanowebappaab1modunAAB1_0100_PUT_IP = "com.dano.web.app.aa.b1.modun.AAB1_0100_PUT_IP";

  public static String comdanowebappaab1modunAAB1_0100_UPDATE_IP = "com.dano.web.app.aa.b1.modun.AAB1_0100_UPDATE_IP";

  public static String comgoogleemailsettingfinduser = "com.google.email.setting.finduser";

  public static String comgoogleemailsettinggetlogsendmail = "com.google.email.setting.getlogsendmail";

  public static String comgoogleemailsettingmaillogs = "com.google.email.setting.maillogs";

  public static String comsystemapisevicegetDTZSAP01 = "com.system.apisevice.getDTZSAP01";

  public static String comsystemapisevicegetDTZSAP02 = "com.system.apisevice.getDTZSAP02";

  public static String comsystemapisevicegetDTZSAP03 = "com.system.apisevice.getDTZSAP03";

  public static String comsystemapisevicegetURI = "com.system.apisevice.getURI";

  public static String comsystemapisevicegetURI_PATH = "com.system.apisevice.getURI_PATH";

  public static String comsystemapisevicesetDTZSAP01 = "com.system.apisevice.setDTZSAP01";

  public static String comsystemapisevicesetDTZSAP02 = "com.system.apisevice.setDTZSAP02";

  public static String comsystemapisevicesetDTZSAP03 = "com.system.apisevice.setDTZSAP03";

  public static String comsysteminsertAPI_PARTNER = "com.system.insert.API_PARTNER";

  public static String comsystemqueryAPI_PARTNER = "com.system.query.API_PARTNER";

  public static String comsystemquerystatusbatch = "com.system.query.status.batch";

  public static String comsystemselectstatusbatchAll = "com.system.select.status.batchAll";

  public static String comsystemselecthost = "com.system.selecthost";

  public static String comsystemselectprimary_Table = "com.system.selectprimary_Table";

  public static String comsystemselecttablename = "com.system.selecttablename";

  public static String comsystemupdateAPI_PARTNER = "com.system.update.API_PARTNER";

  public static String comsystemupdatestatusbatch = "com.system.update.status.batch";

  public static String comtelegramtemplate = "com.telegram.template";

  public static String comtelegramtemplateaddaccount = "com.telegram.template.addaccount";

  public static String comtelegramtemplateaddgroup = "com.telegram.template.addgroup";

  public static String comtelegramtemplateadduser = "com.telegram.template.adduser";

  public static String comtelegramtemplatecheckusertoken = "com.telegram.template.checkusertoken";

  public static String comtelegramtemplatefindbotusername = "com.telegram.template.findbotusername";

  public static String comtelegramtemplatefindchatid = "com.telegram.template.findchatid";

  public static String comtelegramtemplatefindgroup = "com.telegram.template.findgroup";

  public static String comtelegramtemplatefinduser = "com.telegram.template.finduser";

  public static String comtelegramtemplategetDomain = "com.telegram.template.getDomain";

  public static String comtelegramtemplategetfileIdfromdb = "com.telegram.template.getfileIdfromdb";

  public static String comtelegramtemplategetlisttokenbot = "com.telegram.template.getlisttokenbot";

  public static String comtelegramtemplategetmenuwebsite = "com.telegram.template.getmenuwebsite";

  public static String comtelegramtemplategetVideoFromFileID = "com.telegram.template.getVideoFromFileID";

  public static String comtelegramtemplateinsertfileIdfromdb = "com.telegram.template.insertfileIdfromdb";

  public static String comtelegramtemplateinsertjson = "com.telegram.template.insertjson";

  public static String comtelegramtemplateinsertsession = "com.telegram.template.insertsession";

  public static String comtelegramtemplateresigntoken = "com.telegram.template.resigntoken";

  public static String comtelegramtemplateselectfilefromdb = "com.telegram.template.selectfilefromdb";

  public static String comtelegramtemplateselectfilefromdb1 = "com.telegram.template.selectfilefromdb1";

  public static String comtelegramtemplateselectsession = "com.telegram.template.selectsession";

  public static String comtelegramtemplateselectVideo = "com.telegram.template.selectVideo";

  public static String comtelegramtemplatesendfile = "com.telegram.template.sendfile";

  public static String comtelegramtemplateupdatefileIdfromdb = "com.telegram.template.updatefileIdfromdb";

  public static String comtelegramtemplateupdatesession = "com.telegram.template.updatesession";

  public static String comtelegramtemplateupdatetableData = "com.telegram.template.updatetableData";

  public synchronized void SQL_STRING() {
  }
}
